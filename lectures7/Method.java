package lectures7;

import java.util.Scanner;

public class Method {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        methodWithParams("Ivan", 15, "Sofia");
        String response = returnname("Pesho", 15);
        System.out.println(response);

    }
    static void methodWithParams(String name, int age, String city){
        System.out.println(name);
        System.out.println(age);
        System.out.println(city);
    }
    static String returnname (String name, int age) {
        return name +":" + age;

    }
}
