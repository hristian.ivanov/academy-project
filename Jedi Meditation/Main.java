class Jedi {
    String type;
    String level;

    Jedi(String type, String level) {
        this.type = type;
        this.level = level;
    }

    @Override
    public String toString() {
        return type + level;
    }
}
