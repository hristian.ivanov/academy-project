import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class JediMeditation {public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    int n = Integer.parseInt(scanner.nextLine());

    List<Jedi> masters = new ArrayList<>();
    List<Jedi> knights = new ArrayList<>();
    List<Jedi> padawans = new ArrayList<>();
    List<Jedi> toshkoAndSlav = new ArrayList<>();

    for (int i = 0; i < n; i++) {
        String[] jedis = scanner.nextLine().split(" ");
        for (String jedi : jedis) {
            String type = jedi.substring(0, 1);
            String level = jedi.substring(1);

            switch (type) {
                case "m":
                    masters.add(new Jedi(type, level));
                    break;
                case "k":
                    knights.add(new Jedi(type, level));
                    break;
                case "p":
                    padawans.add(new Jedi(type, level));
                    break;
                case "t":
                case "s":
                    toshkoAndSlav.add(new Jedi(type, level));
                    break;
                case "y":
                    break;
                default:
                    throw new IllegalArgumentException("Unknown Jedi type: " + type);
            }
        }
    }

    StringBuilder output = new StringBuilder();

    for (Jedi jedi : masters) {
        output.append(jedi).append(" ");
    }

    for (Jedi jedi : knights) {
        output.append(jedi).append(" ");
    }

    for (Jedi jedi : toshkoAndSlav) {
        output.append(jedi).append(" ");
    }

    for (Jedi jedi : padawans) {
        output.append(jedi).append(" ");
    }

    System.out.println(output.toString().trim());
}
}
