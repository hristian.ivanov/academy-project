import java.util.*;

public class Count_real_number {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

    }


    public static void ex_1() {
        Scanner scanner = new Scanner(System.in);
        Map<Double, Integer> numberCount = new HashMap<>();

        System.out.println("Enter numbers (type 'done' to finish):");
        while (scanner.hasNext()) {
            if (scanner.hasNextDouble()) {
                double number = scanner.nextDouble();
                numberCount.put(number, numberCount.getOrDefault(number, 0) + 1);
            } else if (scanner.hasNext()) {
                scanner.next();
                break;
            } else {

                scanner.next();
            }
        }

        List<Double> sortedNumbers = new ArrayList<>(numberCount.keySet());
        Collections.sort(sortedNumbers);

        for (double number : sortedNumbers) {
            System.out.println(number + " -> " + numberCount.get(number));
        }
    }

    public static void ex_2() {
        Scanner scanner = new Scanner(System.in);
        LinkedHashMap<String, ArrayList<String>> synonymsMap = new LinkedHashMap<>();

        int n = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < n; i++) {

            String word = scanner.nextLine();
            String synonym = scanner.nextLine();

            synonymsMap.putIfAbsent(word, new ArrayList<>());

            synonymsMap.get(word).add(synonym);
        }

        for (Map.Entry<String, ArrayList<String>> entry : synonymsMap.entrySet()) {
            String word = entry.getKey();
            ArrayList<String> synonyms = entry.getValue();

            String synonymsList = String.join(", ", synonyms);
            System.out.println(word + " - " + synonymsList);
        }
    }

    public static void ex_3() {
        Scanner scanner = new Scanner(System.in);
        String[] words = scanner.nextLine().toLowerCase().split("\\s+");
        Map<String, Integer> Count = new LinkedHashMap<>();
        for (String word : words) {
            Count.put(word, Count.getOrDefault(word, 0) + 1);
        }
        List<String> finish = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : Count.entrySet()) {
            if (entry.getValue() % 2 != 0) {
                finish.add(entry.getKey());
            }

        }

        System.out.println(String.join(", ", finish));
    }

    public static void ex_4() {
        Scanner scanner = new Scanner(System.in);
        String[] things = scanner.nextLine().split("\\s+");

        for (String thing : things) {
            if (things.length % 2 == 0) {
                System.out.println(thing);

            }
        }
    }
}


