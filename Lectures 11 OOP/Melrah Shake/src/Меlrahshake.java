public class Меlrahshake {
    public String inputString;
    public String pattern;

    public Меlrahshake(String inputString, String patern) {
        this.inputString = inputString;
        this.pattern = patern;
    }
    public void shake() {
        while (true) {
            int firstMatch = inputString.indexOf(pattern);
            int lastMatch = inputString.lastIndexOf(pattern);

            if (firstMatch != -1 && lastMatch != -1 && firstMatch != lastMatch) {
                StringBuilder sb = new StringBuilder(inputString);
                sb.delete(lastMatch, lastMatch + pattern.length());
                sb.delete(firstMatch, firstMatch + pattern.length());
                inputString = sb.toString();

                System.out.println("Shaked it.");

                int middleIndex = pattern.length() / 2;
                pattern = new StringBuilder(pattern).deleteCharAt(middleIndex).toString();

                if (pattern.isEmpty()) {
                    System.out.println("No shake.");
                    System.out.println(inputString);
                    break;
                }
            } else {
                System.out.println("No shake.");
                System.out.println(inputString);
                break;
            }
        }
    }




}
