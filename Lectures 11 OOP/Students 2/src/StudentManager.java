import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class StudentManager {
    private List<Student2> students = new ArrayList<>();

    public void addOrUpdateStudent(Student2 student) {
        boolean exists = false;
        for (Student2 s : students) {
            if (s.getFirstName().equals(student.getFirstName()) && s.getLastName().equals(student.getLastName())) {
                s.age = student.getAge(); 
                s.city = student.getCity(); 
                exists = true;
                break;
            }
        }
        if (!exists) {
            students.add(student);
        }
    }

   
    public List<String> getStudentsByCity(String city) {
        return students.stream()
                .filter(student -> student.getCity().equals(city))
                .map(student -> student.getFirstName() + " " + student.getLastName() + " is " + student.getAge() + " years old")
                .collect(Collectors.toList());
    }
}
