import java.util.List;

private static void printclass(List<Dragon> dragons, int indent) {
    for (Dragon dragon : dragons)
        if (dragon.isAlive()) {
            System.out.println("  ".repeat(indent) + dragon.getName());
            Dragon.printDragons(dragon.getChildren(), indent + 1);
        }}

public void main() {
}

