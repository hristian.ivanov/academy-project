import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DragonEraSimulation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        List<Dragon> dragons = new ArrayList<>();
        List<Dragon> hatchery = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int startingEggs = scanner.nextInt();
            Dragon parentDragon = new Dragon(null);
            dragons.add(parentDragon);
            for (int j = 0; j < startingEggs; j++) {
                hatchery.add(new Dragon(parentDragon.getName()));
            }
        }

        int y = scanner.nextInt();
        YearType[] yearTypes = new YearType[y];

        for (int i = 0; i < y; i++) {
            String yearType = scanner.next();
            if (yearType.equals("Bad")) {
                yearTypes[i] = new BadYear();
            } else if (yearType.equals("Normal")) {
                yearTypes[i] = new NormalYear();
            } else {
                yearTypes[i] = new GoodYear();
            }
        }

        for (int year = 0; year < y; year++) {
            for (Dragon dragon : dragons) {
                dragon.ageOneYear();
                if (dragon.shouldDie()) {
                }
            }

            for (Dragon dragon : dragons) {
                if (dragon.isAlive()) {
                    dragon.layEggs(hatchery, yearTypes[year]);
                }
            }

            int hatchMultiplier = yearTypes[year].getHatchMultiplier();
            List<Dragon> newDragons = new ArrayList<>();
            for (int i = 0; i < hatchery.size(); i++) {
                Dragon egg = hatchery.get(i);
                if (egg != null && egg.getAge() == 2) {
                    for (int j = 0; j < hatchMultiplier; j++) {
                        Dragon childDragon = new Dragon(egg.getName());
                        newDragons.add(childDragon);
                        for (Dragon parent : dragons) {
                            if (parent.getName().equals(egg.getName())) {
                                parent.addChild(childDragon);
                                break;
                            }
                        }
                    }
                    hatchery.remove(i);
                    i--;
                }
            }
            dragons.addAll(newDragons);
        }

        Dragon.printDragons(dragons, 0);
    }
}
