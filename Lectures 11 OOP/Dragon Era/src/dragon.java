import java.util.ArrayList;
import java.util.List;

class Dragon {
    private static int dragonCount = 0;
    private final int id;
    private String name;
    private int age;
    private final List<Dragon> children;

    public Dragon(String parentName) {
        this.id = ++dragonCount;
        this.name = "Dragon_" + id;
        this.age = 0;
        this.children = new ArrayList<>();
        if (parentName != null) {
            this.name = parentName + "/" + this.name;
        }
    }

    public void ageOneYear() {
        age++;
    }

    public boolean canLayEggs() {
        return age == 3 || age == 4;
    }

    public boolean isAlive() {
        return age < 6;
    }

    public void layEggs(List<Dragon> hatchery, YearType yearType) {
        if (canLayEggs()) {
            int eggsToLay = 1; 
            for (int i = 0; i < eggsToLay; i++) {
                hatchery.add(new Dragon(name)); 
            }
        }
    }

    public List<Dragon> getChildren() {
        return children;
    }

    public void addChild(Dragon child) {
        children.add(child);
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean shouldDie() {
        return age == 6;
    }
    public static void printDragons(List<Dragon> dragons, int i) {
    }
}