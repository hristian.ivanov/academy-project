abstract class YearType {
    public abstract int getHatchMultiplier();
}

class BadYear extends YearType {
    public int getHatchMultiplier() { return 0; }
}

class NormalYear extends YearType {
    public int getHatchMultiplier() { return 1; }
}

class GoodYear extends YearType {
    public int getHatchMultiplier() { return 2; }
}

