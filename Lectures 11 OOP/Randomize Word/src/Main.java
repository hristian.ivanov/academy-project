import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Въведете списък от думи, разделени с интервали:");
        String input = scanner.nextLine();

        RandomizeWords wordRandomizer = new RandomizeWords(input);

        wordRandomizer.randomize();

        System.out.println("Размесените думи:");
        wordRandomizer.printWords();

        scanner.close();
    }
}