import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomizeWords {
    private List<String> words;

    public RandomizeWords (String input) {
        this.words = new ArrayList<>();
        Collections.addAll(this.words, input.split(" "));
    }

    public void randomize() {
        Collections.shuffle(words);
    }

    public void printWords() {
        for (String word : words) {
            System.out.println(word);
        }
    }
}