class EdelFlower {
    private int weissDust;
    private boolean isEternallyBloomed;

    public EdelFlower(int weissDust) {
        this.weissDust = weissDust;
        this.isEternallyBloomed = false;
    }

    public int getWeissDust() {
        return weissDust;
    }

    public boolean isEternallyBloomed() {
        return isEternallyBloomed;
    }

    public void water(int amount) {
        if (!isEternallyBloomed) {
            if (weissDust > amount) {
                weissDust -= amount;
            } else if (weissDust == amount) {
                isEternallyBloomed = true;
            } else {
                weissDust = 0;
            }
        }
    }

    public boolean isBloomed() {
        return weissDust <= 0;
    }

    @Override
    public String toString() {
        return String.valueOf(weissDust);
    }
}
