class WaterBucket {
    private int waterAmount;

    public WaterBucket(int waterAmount) {
        this.waterAmount = waterAmount;
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public void addWater(int amount) {
        waterAmount += amount;
    }
}
