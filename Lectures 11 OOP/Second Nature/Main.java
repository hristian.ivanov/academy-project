import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] flowerInput = scanner.nextLine().split(" ");
        List<EdelFlower> flowers = new ArrayList<>();
        for (String flower : flowerInput) {
            flowers.add(new EdelFlower(Integer.parseInt(flower)));
        }

        String[] bucketInput = scanner.nextLine().split(" ");
        List<WaterBucket> buckets = new ArrayList<>();
        for (String bucket : bucketInput) {
            buckets.add(new WaterBucket(Integer.parseInt(bucket)));
        }

        EdelFlowerBlooming blooming = new EdelFlowerBlooming(flowers, buckets);
        blooming.waterFlowers();
        blooming.printResults();

        scanner.close();
    }
}