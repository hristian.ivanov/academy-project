import java.util.ArrayList;
import java.util.List;

class EdelFlowerBlooming {
    private List<EdelFlower> flowers;
    private List<WaterBucket> buckets;
    private List<EdelFlower> secondNatureFlowers;

    public EdelFlowerBlooming(List<EdelFlower> flowers, List<WaterBucket> buckets) {
        this.flowers = flowers;
        this.buckets = new ArrayList<>();
        this.secondNatureFlowers = new ArrayList<>();


        for (int i = buckets.size() - 1; i >= 0; i--) {
            this.buckets.add(buckets.get(i));
        }
    }

    public void waterFlowers() {
        for (EdelFlower flower : flowers) {
            while (!buckets.isEmpty() && !flower.isBloomed()) {
                WaterBucket bucket = buckets.remove(buckets.size() - 1);
                int waterAmount = bucket.getWaterAmount();

                if (waterAmount > 0) {
                    flower.water(waterAmount);
                    if (waterAmount > flower.getWeissDust()) {
                        int remainingWater = waterAmount - flower.getWeissDust();
                        if (!buckets.isEmpty()) {
                            buckets.get(buckets.size() - 1).addWater(remainingWater);
                        }
                    } else if (waterAmount == flower.getWeissDust()) {
                        secondNatureFlowers.add(flower);
                    }
                }
            }
        }
    }

    public void printResults() {

        if (!buckets.isEmpty()) {
            for (WaterBucket bucket : buckets) {
                System.out.print(bucket.getWaterAmount() + " ");
            }
            System.out.println();
        } else {
            boolean hasRemainingFlowers = false;
            for (EdelFlower flower : flowers) {
                if (!flower.isBloomed()) {
                    System.out.print(flower.getWeissDust() + " ");
                    hasRemainingFlowers = true;
                }
            }
            if (hasRemainingFlowers) {
                System.out.println();
            } else {
                System.out.println("None");
            }
        }


        if (secondNatureFlowers.isEmpty()) {
            System.out.println("None");
        } else {
            for (EdelFlower flower : secondNatureFlowers) {
                System.out.print(flower.getWeissDust() + " ");
            }
            System.out.println();
        }
    }
}