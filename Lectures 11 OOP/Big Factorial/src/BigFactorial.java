import java.math.BigInteger;

class BigFactorial {
    private int number;

    public BigFactorial(int number) {
        this.number = number;
    }

    public BigInteger calculate() {
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= number; i++) {
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }
}
