import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class SongManager {
    private List<Song> songs = new ArrayList<>();

    public void addSong(Song song) {
        songs.add(song);
    }

    public List<String> getSongsByType(String type) {
        return songs.stream()
                .filter(song -> type.equals("all") || song.getTypeList().equals(type))
                .map(Song::getName)
                .collect(Collectors.toList());
    }
}