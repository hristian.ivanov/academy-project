import java.util.List;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        SongManager songManager = new SongManager();

        for (int i = 0; i < n; i++) {
            String[] input = scanner.nextLine().split("_");
            songManager.addSong(new Song(input[0], input[1], input[2]));
        }

        String typeList = scanner.nextLine();
        List<String> foundSongs = songManager.getSongsByType(typeList);
        foundSongs.forEach(System.out::println);
        scanner.close();
    }
}