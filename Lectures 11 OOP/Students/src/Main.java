import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StudentManager studentManager = new StudentManager();

        while (true) {
            String input = scanner.nextLine();
            if (input.equals("end")) {
                break;
            }
            String[] data = input.split(" ");
            String firstName = data[0];
            String lastName = data[1];
            int age = Integer.parseInt(data[2]);
            String city = data[3];

            studentManager.addOrUpdateStudent(new Student(firstName, lastName, age, city));
        }

        String targetCity = scanner.nextLine();
        List<String> foundStudents = studentManager.getStudentsByCity(targetCity);
        foundStudents.forEach(System.out::println);
        scanner.close();
    }
}