import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class StudentManager {
    private List<Student> students = new ArrayList<>();

    public void addOrUpdateStudent(Student student) {
        students.removeIf(s -> s.getFirstName().equals(student.getFirstName()) && s.getLastName().equals(student.getLastName()));
        students.add(student);
    }

    public List<String> getStudentsByCity(String city) {
        return students.stream()
                .filter(student -> student.getCity().equals(city))
                .map(student -> student.getFirstName() + " " + student.getLastName() + " is " + student.getAge() + " years old")
                .collect(Collectors.toList());
    }
}