import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.nextLine();
        String second = scanner.nextLine();

        SumBigNumbers bigNumberSum = new SumBigNumbers(first, second);
        System.out.println(bigNumberSum.getSum());
        scanner.close();
    }
    }
