import java.math.BigInteger;

class SumBigNumbers {
     private BigInteger firstNumber;
     private BigInteger secondNumber;

     public SumBigNumbers(String first, String second) {
         this.firstNumber = new BigInteger(first);
         this.secondNumber = new BigInteger(second);
     }

     public BigInteger getSum() {
         return firstNumber.add(secondNumber);
     }

}
