import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CubicArtillery {
    String name;
    Queue<Integer> Weapons = new LinkedList<>();
    int capacity = 0;

    CubicArtillery(String name) {
        this.name = name;
    }

    boolean isThereSpace(int WeaponCapacity, int MaxCapacity) {
        return (capacity + WeaponCapacity <= MaxCapacity);
    }

    void addWeapon(int weaponCapacity) {
        Weapons.add(weaponCapacity);
        capacity += weaponCapacity;
    }

    void removeWeapon() {
        if (!Weapons.isEmpty()) {
            int removedWeapon = Weapons.poll();
            capacity -= removedWeapon;
        }
    }

    public void printBunker() {
        System.out.print(name + " -> ");
        if (Weapons.isEmpty()) {
            System.out.println("Empty");
        } else {
            System.out.println(String.join(", ", Weapons.stream()
                    .map(String::valueOf).toArray(String[]::new)));
        }
    }
    public static void processWeapon(int weaponCapacity, int maxCapacity, List<CubicArtillery> bunkerList) {
        Iterator<CubicArtillery> iterator = bunkerList.iterator();

        while (iterator.hasNext()) {
            CubicArtillery currentBunker = iterator.next();

            if (currentBunker.isThereSpace(weaponCapacity, maxCapacity)) {
                currentBunker.addWeapon(weaponCapacity);
                return;
            } else if (!iterator.hasNext()) {
                if (weaponCapacity <= maxCapacity) {
                    while (!currentBunker.isThereSpace(weaponCapacity, maxCapacity)) {
                        currentBunker.removeWeapon();
                    }
                    currentBunker.addWeapon(weaponCapacity);
                }
            } else {
                currentBunker.printBunker();
                iterator.remove();
            }
        }
    }
}
