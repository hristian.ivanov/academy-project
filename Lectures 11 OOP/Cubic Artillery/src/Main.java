import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxCapacity = Integer.parseInt(scanner.nextLine());
        List<CubicArtillery> bunkerList = new LinkedList<>();

        String input;
        while (!(input = scanner.nextLine()).equals("Bunker Revision")) {
            String[] tokens = input.split(" ");
            for (String token : tokens) {
                if (Character.isLetter(token.charAt(0))) {
                    bunkerList.add(new CubicArtillery(token));
                } else {
                    int weaponCapacity = Integer.parseInt(token);
                    CubicArtillery.processWeapon(weaponCapacity, maxCapacity, bunkerList);
                }
            }
        }
    
    }


}