import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxCapacity = scanner.nextInt();
        List<Bunker> bunkersList = new ArrayList<>();

        scanner.nextLine();

        while (true) {
            String input = scanner.nextLine();
            if (input.equals("Bunker Revision")) {
                break;
            }

            String[] elements = input.split(" ");
            for (String element : elements) {
                if (!element.isEmpty() && Character.isLetter(element.charAt(0))) {
                    bunkersList.add(new Bunker(maxCapacity, element, new ArrayList<>()));
                } else if (!element.isEmpty() && Character.isDigit(element.charAt(0))) {
                    int weapon = Integer.parseInt(element);
                    boolean weaponStored = false;

                    for (Bunker bunker : bunkersList) {
                        if (bunker.AddWeapon(weapon)) {
                            weaponStored = true;
                            break;
                        }
                    }

                    if (!weaponStored) {
                        Bunker firstBunker = bunkersList.get(0);
                        if (weapon <= firstBunker.capacity) {
                            firstBunker.removeWeaponUntilFit(weapon);
                            firstBunker.AddWeapon(weapon);
                        }
                    }
                }
            }
        }

        for (Bunker bunker : bunkersList) {
            bunker.print();
        }

        scanner.close();
    }
}
