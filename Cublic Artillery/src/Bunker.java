import java.util.List;

public class Bunker {
    public int capacity;
    private String name;
    private List<Integer> weapons;
    private int weaponsCount = 0;
    private int leftOverflow = 0;

    public Bunker(int capacity, String name, List<Integer> weapons) {
        this.capacity = capacity;
        this.name = name;
        this.weapons = weapons;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getWeapons() {
        return weapons;
    }

    public int getWeaponsCount() {
        return weaponsCount;
    }

    public int getLeftOverflow() {
        return leftOverflow;
    }


    public Bunker Add(int weapon) { // 20
        if (capacity >= weaponsCount) {
            print();
        } else {
            int weaponsCountTemp = weaponsCount + weapon; // 30 + 20 = 50

            if (weaponsCountTemp <= capacity) {
                // have capacity
                weapons.add(weapon); // 30, 20
                weaponsCount += weapon; // 50
            } else {
                // don't have capacity

                // weapon 40
                // int weaponsCountTemp = weaponsCount + weapon; // 30 + 40 = 70

                leftOverflow = (capacity - weaponsCountTemp) * -1; // 60 - 70 = -10
                weapons.add(weapon - leftOverflow);
                weaponsCount += weapon - leftOverflow;
            }

        }

        return this;
    }

    public boolean CanFitWeapon(int weapon) {
        return (weaponsCount + weapon) <= capacity;
    }

    public boolean AddWeapon(int weapon) {
        if (weaponsCount + weapon <= capacity) {
            weapons.add(weapon);
            weaponsCount += weapon;
            return true;
        }

        return false; // nothing is added
    }
    public void removeWeaponUntilFit(int weapon){
        while (!weapons.isEmpty() && weaponsCount + weapon > capacity) {
            weaponsCount -= weapons.remove(0);
        }
    }
    public boolean isEmpty(){
        return weapons.isEmpty();
    }

    public void removeOldestWeapon() {
        if (!weapons.isEmpty()) {
            weaponsCount -= weapons.remove(0);
        }
    }

    public void print () {
        if (weapons.isEmpty()) {
            System.out.println( name + " -> Empty");
        } else {
            System.out.println( name + " -> " + weapons);
        }
    }


}
