package lectures4;

import java.util.Scanner;

public class for_loop {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        int n = scanner.nextInt();

        int sumEven = 0;
        int sumOdd = 0;
        for (int i = 1; i <= n; i++){
            int input = scanner.nextInt();
            if (i % 2 == 0){
                System.out.println("Even");
            }else {
                System.out.println("Odd");
            }
        }

    }

    public static void ex() {
        for ( int i = 0; i <= 100; i++) {
            System.out.println(i);
        }
        // ex 2
//        Scanner scanner =new Scanner(System.in);
//        int input = scanner.nextInt();
//
//        for(int i = input; i > 0; i--) {
//            System.out.println(i);
        // ex3
//
//        for(int i = 1; i < input;  i +=3){
//            System.out.println(i);
//        }
        //ex4
//        String input = scanner.next();
//
//
//        for (char element : input.toCharArray()) {
//            System.out.println(element);
//        }

        // ex5
//        int n = scanner.nextInt();
//
//        Integer maxNumber;
//        Integer minNumber;
//        for(int i = 1; n <= i; i++){
//            int input = scanner.nextInt();
//
//            if(maxNumber == null || maxNumber < input){
//                maxNumber = input;
//            }
//            if(minNumber == null || minNumber > input) {
//                minNumber = input;
//            }
//        }
//        System.out.println("Max number: " + maxNumber);
//        System.out.println("Min number: " + minNumber );
        // ex 6
//        Scanner scanner =new Scanner(System.in);
//        int n = scanner.nextInt();
//
//        int sumEven = 0;
//        int sumOdd = 0;
//        for (int i = 1; i <= n; i++){
//            int input = scanner.nextInt();
//            if (i % 2 == 0){
//                System.out.println("Even");
//            }else {
//                System.out.println("Odd");
//            }
//        }

    }

}
