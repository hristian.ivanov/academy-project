package lectures5;

import java.util.Scanner;

public class Homework {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


    }
    public static void ex1(){
        Scanner scanner = new Scanner(System.in);
        String bookName = scanner.nextLine();
        int booksChecked = 0;
        boolean isFound = false;

        while (true) {
            String currentBook = scanner.nextLine();

            if (currentBook.equals("No More Books")) {
                break;
            }

            if (currentBook.equals(bookName)) {
                isFound = true;
                break;
            }

            booksChecked++;
        }

        if (isFound) {
            System.out.printf("You checked %d books and found it.%n", booksChecked);
        } else {
            System.out.printf("The book you search is not here!%nYou checked %d books.%n", booksChecked);
        }
    }


    public static void ex2(){
        Scanner scanner = new Scanner(System.in);
        int maxPoorGrades = Integer.parseInt(scanner.nextLine());
        int poorGradesCount = 0;
        int totalProblems = 0;
        int sumGrades = 0;
        String lastProblem = "";
        boolean isFailed = false;

        while (poorGradesCount < maxPoorGrades) {
            String problemName = scanner.nextLine();

            if (problemName.equals("Enough")) {
                isFailed = true;
                break;
            }

            int grade = Integer.parseInt(scanner.nextLine());
            sumGrades += grade;
            totalProblems++;
            lastProblem = problemName;

            if (grade <= 4) {
                poorGradesCount++;
            }
        }

        if (isFailed) {
            double averageScore = (double) sumGrades / totalProblems;
            System.out.printf("Average score: %.2f%n", averageScore);
            System.out.printf("Number of problems: %d%n", totalProblems);
            System.out.printf("Last problem: %s%n", lastProblem);
        } else {
            System.out.printf("You need a break, %d poor grades.%n", poorGradesCount);
        }
    }


    public static void ex4(){
        Scanner scanner = new Scanner(System.in);
        final int goal = 10000;
        int totalSteps = 0;

        while (totalSteps < goal) {
            String input = scanner.nextLine();

            if (input.equals("Going home")) {
                int stepsToHome = Integer.parseInt(scanner.nextLine());
                totalSteps += stepsToHome;
                break;
            } else {
                int steps = Integer.parseInt(input);
                totalSteps += steps;
            }
        }

        if (totalSteps >= goal) {
            System.out.printf("Goal reached! Good job!%n%d steps over the goal!%n", totalSteps - goal);
        } else {
            System.out.printf("%d more steps to reach goal.%n", goal - totalSteps);
        }
    }


    public static void ex3(){
        Scanner scanner = new Scanner(System.in);

        double vacationMoney = Double.parseDouble(scanner.nextLine());
        double currentMoney = Double.parseDouble(scanner.nextLine());
        int days = 0;
        int spendDays = 0;

        while (currentMoney < vacationMoney && spendDays < 5) {
            String action = scanner.nextLine();
            double amount = Double.parseDouble(scanner.nextLine());
            days++;

            if (action.equals("save")) {
                currentMoney += amount;
                spendDays = 0;
            } else if (action.equals("spend")) {
                currentMoney -= amount;
                if (currentMoney < 0) {
                    currentMoney = 0;
                }
                spendDays++;
            }
        }

        if (spendDays == 5) {
            System.out.printf("You can't save the money.%n%d%n", days);
        } else {
            System.out.printf("You saved the money for %d days.%n", days);
        }

    }


    public static void ex6(){
        Scanner scanner = new Scanner(System.in);

        int width = Integer.parseInt(scanner.nextLine());
        int length = Integer.parseInt(scanner.nextLine());
        int totalPieces = width * length;

        while (totalPieces > 0) {
            String input = scanner.nextLine();

            if (input.equals("STOP")) {
                break;
            }

            int piecesTaken = Integer.parseInt(input);
            totalPieces -= piecesTaken;
        }

        if (totalPieces < 0) {
            System.out.printf("No more cake left! You need %d pieces more.%n", Math.abs(totalPieces));
        } else {
            System.out.printf("%d pieces are left.%n", totalPieces);
        }

    }


    public static void ex5(){
        Scanner scanner = new Scanner(System.in);
        double change = Double.parseDouble(scanner.nextLine());
        int coinsCount = 0;
        int changeInCents = (int) Math.round(change * 100);

        while (changeInCents > 0) {
            if (changeInCents >= 200) {
                changeInCents -= 200;
            } else if (changeInCents >= 100) {
                changeInCents -= 100;
            } else if (changeInCents >= 50) {
                changeInCents -= 50;
            } else if (changeInCents >= 20) {
                changeInCents -= 20;
            } else if (changeInCents >= 10) {
                changeInCents -= 10;
            } else if (changeInCents >= 5) {
                changeInCents -= 5;
            } else if (changeInCents >= 2) {
                changeInCents -= 2;
            } else if (changeInCents >= 1) {
                changeInCents -= 1;
            }
            coinsCount++;
        }

        System.out.println(coinsCount);
    }

    public static void ex7(){
        Scanner scanner = new Scanner(System.in);

        int width = Integer.parseInt(scanner.nextLine());
        int length = Integer.parseInt(scanner.nextLine());
        int height = Integer.parseInt(scanner.nextLine());
        int availableSpace = width * length * height;

        while (availableSpace > 0) {
            String input = scanner.nextLine();

            if (input.equals("Done")) {
                break;
            }

            int boxes = Integer.parseInt(input);
            availableSpace -= boxes;
        }

        if (availableSpace < 0) {
            System.out.printf("No more free space! You need %d Cubic meters more.%n", Math.abs(availableSpace));
        } else {
            System.out.printf("%d Cubic meters left.%n", availableSpace);
        }
    }

}



