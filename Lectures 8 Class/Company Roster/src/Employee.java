public class Employee {

    private String name;
    private double salary;
    private String position;
    private String department;
    private String email;
    private int age;

    public Employee(String name, double salary, String position, String department, String email, int age) {
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.email = email;
        this.age = age;
    }

    public Employee(String name, double salary, String position, String department){
        this(name, salary, position, department, "n/a", -1);
    }

    public String getName() {
        return name;
    }


    public double getSalary() {
        return salary;
    }


    public String getPosition() {
        return position;
    }


    public String getDepartment() {
        return department;
    }


    public String getEmail() {
        return email;
    }


    public int getAge() {
        return age;
    }



   public void print(){
       System.out.printf("Name: %s, Salary: %.2f, Position: %s, Department: %s, Email: %s, Age: %s%n",
               name, salary, position, department, email, (age == -1 ? -1 : age));
   }
    public void getInfo(){
        System.out.printf("Name: %s, Salary: %.2f, Position: %s, Department: %s, Email: %s, Age: %s%n",
                name, salary, position, department, email, (age == -1 ? -1 : age));
    }

    public double getSalaryAfterExpenses(double expenses) {
        return this.salary - expenses;
    }

    public String promote(String newPosition, double newSalary) {
        this.position = newPosition;
        this.salary = newSalary;
        return String.format("%s was promoted to %s with salary %.2f", this.name, this.position, this.salary);
    }

    public void decreaseSalary(double decrease) {
        this.salary -= decrease;
    }


}
