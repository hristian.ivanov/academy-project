import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            String name = scanner.next();
            double salary = scanner.nextDouble();
            String position = scanner.next();
            String department = scanner.next();
            System.out.println("If you don't want to write email enter n/a");
            String email = scanner.next();
            System.out.println("If you don't want to enter your age enter 0'");
            int age = scanner.nextInt();

            if (email.equals(" ")){
               email = "n/a";
            } else if (age == 0) {
                age = -1;
            }

            Employee employee = new Employee(name, salary, position, department, email, age);
            employee.print();


            double salaryAfterExpenses = employee.getSalaryAfterExpenses(salary * 10/100);
            System.out.printf("Salary after expenses: %.2f%n", salaryAfterExpenses);

            String promotionMessage = employee.promote("Boss", 3500);
            System.out.println(promotionMessage);

            System.out.println("Enter the amount by which you want to decrease the salary:");
            double decreaseAmount = scanner.nextDouble();
            employee.decreaseSalary(decreaseAmount);
            System.out.printf("Salary after decrease: %.2f%n", employee.getSalary());
        }


        }
    }







