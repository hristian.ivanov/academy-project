public class Car {

        private String brand;
        private String model;
    private int HP;

    public Car(String brand, String model, int HP){
        this.brand = brand;
        this.model = model;
        this.HP = HP;
    }


    public Car(String brand){
        this(brand, "UNKOWN", 0);

    }


    public String getBrand(){
        return brand;
    }


    public void getBrand(String brand){
        this.brand = brand;
    }


    public String getmodel(){
        return model;
    }


    public void getModel(){
        this.model = model;
    }


    public int getHP(){
        return HP;
    }


    public void gethp(){
        this.HP = HP;
    }




    public void print(){
        System.out.printf("The car is: %s %s - %s HP. %n", brand, model, HP);
    }
}
