import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        List<Person> People = new ArrayList<>();
        for(int i = 0; i< n; i ++){
            Person person = new Person(scanner.next(), scanner.nextInt());

            People.add(person);


        }
        People.stream().filter(person -> person.getAge() > 30)
                .forEach(person -> System.out.println(person));
    }
}