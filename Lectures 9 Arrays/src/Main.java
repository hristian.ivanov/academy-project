import java.util.*;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] arr = Arrays.stream(scanner.nextLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        while (true) {
            String command = scanner.nextLine();
            if (command.equals("end")) {
                break;
            }

            String[] parts = command.split(" ");
            String action = parts[0];

            switch (action) {
                case "swap":
                    int index1 = Integer.parseInt(parts[1]);
                    int index2 = Integer.parseInt(parts[2]);
                    int temp = arr[index1];
                    arr[index1] = arr[index2];
                    arr[index2] = temp;
                    break;
                case "multiply":
                    index1 = Integer.parseInt(parts[1]);
                    index2 = Integer.parseInt(parts[2]);
                    arr[index1] = arr[index1] * arr[index2];
                    break;
                case "decrease":

                    for (int i = 0; i < arr.length; i++) {
                        arr[i]--;
                    }
                    break;
            }
        }

        System.out.println(Arrays.toString(arr).replaceAll("[\\[\\]]", "").replaceAll(", ", ","));









    }


    public static void ex_3() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        List<Integer> aray1 = new ArrayList<Integer>();
        List<Integer> aray2 = new ArrayList<Integer>();

        for (int i = 0; i < n; i++) {
            int first = scanner.nextInt();
            int second = scanner.nextInt();

            if (i % 2 != 0) {
                aray1.add(first);
                aray2.add(second);
            } else {
                aray1.add(second);
                aray2.add(first);
            }
        }
        System.out.println(aray1);
        System.out.println(aray2);
    }

    public static void ex_1() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int sum = 0;
        List<Integer> wagon = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            wagon.add(scanner.nextInt());
        }
        for (int number : wagon) {
            sum += number;
        }
        System.out.println(wagon);
        System.out.println(sum);

    }

    public static void ex_2() {
        Scanner scanner = new Scanner(System.in);

        String[] array1 = scanner.nextLine().split("");
        String[] array2 = scanner.nextLine().split("");

        List<String> result = new ArrayList<>();
        List<String> common = new ArrayList<>();

        for (String element : array1) {
            result.add(element);
        }

        for (String element : array2) {
            if (result.contains(element)) {
                common.add(element);
                result.remove(element);
            }
        }
        for (String element : common) {
            System.out.print(element);
        }
    }

    public static void ex_4() {
        Scanner scanner = new Scanner(System.in);

        String[] n = scanner.nextLine().split("");
        List<String> list = Arrays.asList(n);

        int index = scanner.nextInt();
        int lastElement = list.size() - 1;

        List<String> lastpart = new ArrayList<>();
        for (int i = index; i <= lastElement; i++) {
            lastpart.add(list.get(i));
        }
        System.out.println(lastpart);

        List<String> firstpart = new ArrayList<>();
        for (int i = 0; i < index; i++) {
            firstpart.add(list.get(i));
        }
        System.out.println(firstpart);
    }

    public static void ex_5() {
        Scanner scanner = new Scanner(System.in);

        String[] inputArray = scanner.nextLine().trim().split("\\s+");

        int[] array = new int[inputArray.length];
        for (int i = 0; i < inputArray.length; i++) {
            array[i] = Integer.parseInt(inputArray[i]);
        }

        List<Integer> topIntegers = new ArrayList<>();

        if (array.length > 0) {
            int maxSoFar = array[array.length - 1];
            topIntegers.add(maxSoFar);

            for (int i = array.length - 2; i >= 0; i--) {
                if (array[i] > maxSoFar) {
                    maxSoFar = array[i];
                    topIntegers.add(maxSoFar);
                }
            }
        }

        System.out.println(topIntegers);
    }

    public static void ex_6() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int totalsum = 0;
        int leftsum = 0;

        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        for (int num : array) {
            totalsum += num;
        }
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            if (leftsum == totalsum - leftsum - array[i]) {
                System.out.println(i);
                found = true;
                break;
            }
            leftsum += array[i];
        }

        if (!found) {
            System.out.println("no");
        }
    }

    public static void ex_7() {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        List<Integer> numbers = new ArrayList<>();


        for (int i = 0; i < n; i++) {
            numbers.add(scanner.nextInt());
        }
        while (numbers.size() > 1) {
            List<Integer> result = new ArrayList<>();
            for (int num = 0; num < numbers.size() - 1; num++) {
                result.add(numbers.get(num) + numbers.get(num + 1));
            }
            numbers = result;
        }
        System.out.println(numbers.get(0));
    }

    public static void ex_8() {
        Scanner scanner = new Scanner(System.in);

        List<Integer> numbers = new ArrayList<Integer>();

        System.out.println("Enter a number (If you want stop enter some letter):");
        while (scanner.hasNextInt()) {
            numbers.add(scanner.nextInt());
        }
        scanner.nextLine();

        int targetsum = scanner.nextInt();


        for (int i = 0; i < numbers.size(); i++) {
            for (int j = i + 1; j < numbers.size(); j++) {
                if (numbers.get(i) + numbers.get(j) == targetsum) {
                    System.out.println(numbers.get(i) + " " + numbers.get(j));
                }
            }

        }
    }

}
