public interface Searchable {
    boolean matches(String keyword);
}
