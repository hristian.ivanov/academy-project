import java.util.Scanner;

public class LibraryManagementSystem {
    public static void main(String[] args) {
        Library library = new Library();

        library.addBook(new Book("Rich Father Poor Father", "Robert Kiyosaki", "123456", 2000));
        library.addBook(new Book("1984", "George Orwell", "789101", 1949));
        library.addBook(new Book("Don Quixote", "Miguel de Cervantes ", "202020", 1605));
        library.addBook(new Book("Hristo Stoichkov: Autobiography", "Hristo Stoichkov ", "194802", 2020));
        library.addBook(new Book("Гунди какъвто не го познавате", "Елица Младенова, Георги Иванов, Добромир Добрев, Кристиан Иванов ", "451943", 2018));
        library.addMember(new Member("141698", "Petko", "Petk0@gmail.com"));
        library.addMember(new Member("232267", "Ivan", "Vancho@gmail.com"));
        library.addMember(new Member("090978", "Gosho", "Gogata28@gmail.com"));
        library.addMember(new Member("123456", "Nicola", "Kolio@abv.bg"));
        library.addMember(new Member("789101", "Petur", "Pesho09@gmail.com"));

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Library Management System");
            System.out.println("1. Search Book");
            System.out.println("2. Borrow Book");
            System.out.println("3. Return Book");
            System.out.println("4. Exit");
            System.out.print("Enter choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.print("Enter keyword (title/author): ");
                    String keyword = scanner.nextLine();
                    var results = library.searchBooks(keyword);
                    if (results.isEmpty()) {
                        System.out.println("No books found.");
                    } else {
                        results.forEach(book -> System.out.println(book.getTitle() + " by " + book.getAuthor()));
                    }
                    break;
                case 2:
                    System.out.print("Enter Member ID: ");
                    String memberId = scanner.nextLine();
                    Member member = library.getMemberById(memberId);
                    if (member == null) {
                        System.out.println("Member not found.");
                        break;
                    }
                    System.out.print("Enter Book ISBN: ");
                    String isbn = scanner.nextLine();
                    Book book = library.getBookByIsbn(isbn);
                    if (book == null || !member.borrowBook(book)) {
                        System.out.println("Cannot borrow book.");
                    } else {
                        System.out.println("Book borrowed successfully.");
                    }
                    break;
                case 3:
                    System.out.print("Enter Member ID: ");
                    memberId = scanner.nextLine();
                    member = library.getMemberById(memberId);
                    if (member == null) {
                        System.out.println("Member not found.");
                        break;
                    }
                    System.out.print("Enter Book ISBN: ");
                    isbn = scanner.nextLine();
                    book = library.getBookByIsbn(isbn);
                    if (book == null || !member.returnBook(book)) {
                        System.out.println("Cannot return book.");
                    } else {
                        System.out.println("Book returned successfully.");
                    }
                    break;
                case 4:
                    System.out.println("Exiting...");
                    scanner.close();
                    return;
                default:
                    System.out.println("Invalid choice.");
            }
        }
    }
}
